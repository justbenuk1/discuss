'use client'
import { NextUIProvider } from "@nextui-org/react"
import { ReactNode } from "react"

type ProvidersChildren = {
  children: ReactNode
}
export default function Providers({ children }: ProvidersChildren) {
  return (
    <NextUIProvider>{children}</NextUIProvider>
  )
}
